// file: Graph.java
// date: Sat 2/19/15
//
package part1;

/**
 * @author Arthur Lunn : all3187
 * @since 2/19/16
 * @version 1.1
 * @ Represents a singe unidirectional edge with weight
 *  represents a single unidirectional edge with weight. In order to represent
 *  a bi-directional edge two edges must be used. The starting vertex is not stored.
 */
@SuppressWarnings("unused")
public class Edge{

    private int destination;
    private int weight;

    /**
     * Basic constructor for the edge class.
     *
     * @param _destination destination vertex
     * @param _weight weight of given edge
     */
    public Edge( int _destination, int _weight){
        destination = _destination;
        weight = _weight;
    }

    /**
     * basic getter
     *
     * @return The destination Vertex of this edge
     */
    public int getDestination(){
        return destination;
    }

    public int getWeight(){
        return weight;
    }

    /**
     * basic setter
     * @param _destination The destination to set this edge to
     */
    public void setDestination( int _destination){
        destination = _destination;
    }

    /**
     * basic setter
     * @param _weight The weight to set this edge to
     */
    public void setWeight(int _weight){
        weight = _weight;
    }
}
// file: Graph.java
// date: Sat 2/19/15
//
package part1;

import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.Random;
import java.io.BufferedReader;
import java.lang.Integer;
import java.util.LinkedList;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author Arthur Lunn : all3187
 * @since 2/19/16
 * @version 1.1
 * @ Basic program for generating and displaying a fully connected bi-directional graph textually
 * Given a properly formatted input file this program will use the given
 * data to generate a fully connected bi directional graph. The program will continue
 * to generate graphs until a fully connected graph is achieved.
 */
@SuppressWarnings("unused")
public class Graph{

    // String for various messages
    private static final String testString = "TEST: n=%d, seed=%d, p=%s\n";
    private static final String timeString ="Time to generate the graph: %d" +
                                     " milliseconds\n";
    private static final String matrixString = "The graph as an adjacency matrix:\n";
    private static final String adjString = "The graph as an adjacency list:\n";
    private static final DecimalFormat df = new DecimalFormat("0.#####");
    /**
     * Main method, tasked with reading the input file and generating the graph.
     * @param args A single input file giving the p, seed and n values.
     */
    @SuppressWarnings({"unchecked","ThrowableInstanceNeverThrown"})
    public static void main(String[] args){
        int n = 0, seed = 0;
        double p = 0;
        Random weightRand, edgeRand;
        LinkedList<Edge>[] adj;
        int [][] matrix;
        long genTime;
        BufferedReader inputRdr = null;

        // Open the file and catch applicable exceptions
        try{
            inputRdr = new BufferedReader(new FileReader( args[0] ) );
        }
        catch (FileNotFoundException e){
            System.out.println("Input file not found");
            System.exit(-1);
        }
        // parse out the values from the file catching applicable exceptions
        try{
            n = Integer.parseInt( inputRdr.readLine() );
            seed = Integer.parseInt( inputRdr.readLine() );
        }
        catch (NumberFormatException e) {
            System.out.println("n and seed must be integers");
            System.exit(-3);
        }
        catch (IOException  e){
            System.out.println("permission error with file");
            System.exit(-2);
        }
        //ensure that n is in proper range
        if ( ! ( n > 1 ) ){
            System.out.println("n must be greater than 1");            
            System.exit(-4);
        }
        // Parse out p value and ensure it is an acceptable value
        try{
            p = Double.parseDouble( inputRdr.readLine() );
        }
        catch (NumberFormatException e) {
            System.out.println("p must be a real number");
            System.exit(-5);
        }
        catch (IOException  e){
            System.out.println("permission error with file");
            System.exit(-2);
        }
        if ( p < 0 || p > 1){
            System.out.println("p must be between 0 and 1");            
            System.exit(-6);
        }
        // Initialize the adjacency matrix
        matrix = new int[n][n];
        // Initialize the adjacency list
        adj = new LinkedList[n];
        for(int i = 0; i < n; i++){
            adj[i] = new LinkedList<Edge>();
        }
        // Initialize the Random number generators
        edgeRand = new Random(seed);
        weightRand = new Random( (seed*2) );
        // Start the timer
        genTime = System.nanoTime();
        // Continue to try making the graph until an acceptable graph is found
        while(  matrixDFS(n, false, matrix) < n ){
            createGraphs(n, p, edgeRand, weightRand, adj, matrix);
        }
        // End the timer
        genTime = System.nanoTime() - genTime;
        //convert to milliseconds
        genTime /= 1000000;
        // Print info on the test information and time
        System.out.format( testString, n, seed, df.format(p) );
        System.out.format( timeString, genTime);
        // If there are less than ten vertices print out the adjacency list
        // and adjacency matrix textually
        if( n < 10){
            System.out.print(matrixString);
            printMatrix(n, matrix);
            System.out.print(adjString);
            printAdj(n, adj);
            matrixDFS(n, true, matrix);
        }
    }

    /**
     * Function to assist in the construction of the graph. Given a number of vertices (n)
     * and a probability (p) it will check every pair of vertices and generate a random
     * number. If the number is less than or equal to p those two vertices are then
     * connected with a weight (w) which is generated randomly.
     * @param n The number of vertices
     * @param p The probability of an edge
     * @param edgeRand The RNG for edge probabilities
     * @param weightRand The RNG for weight values
     * @param adj The adjacency list
     * @param matrix The adjacency matrix
     */
    private static void createGraphs(int n, double p, Random edgeRand,
                                     Random weightRand, LinkedList<Edge>[] adj,
                                     int[][] matrix){
        for(int i = 0; i < n; i++){
            for(int j = i+1; j < n; j++){
                if(j != i){
                    if( ! ( edgeRand.nextDouble() > p ) ){
                        matrix[i][j] =  1 + (weightRand.nextInt( ( n  ) ) );
                        matrix[j][i] = matrix[i][j];
                        adj[i].add( new Edge( j, matrix[i][j] ) );
                        adj[j].add(new Edge(i, matrix[i][j]));
                    }
                }
            }
        }
    }

    /**
     * Matrix style DFS. Method does not iterate through all "starting" nodes
     * as this is not necessary to determine if they are connected. Uses a
     * boolean to determine if the results should be printed
     *
     * @param v The number of vertices for the graph
     * @param printResults If true the results will be printed
     * @return The number of vertices visited.
     */
    private static int matrixDFS(int v, boolean printResults, int[][] matrix){
        int totalVisited = 0;
        int[] visited = new int[v];
        String vertString = "";
        String predString = "";
        String dfsString = "Depth-First Search:\nVertices:  \n";
        int[] predecessor = new int[v];
        // Start at Vertex one; this should reach all vertices if connected
        predecessor[0] = -1;
        matrixDFSVisit(v, 0, matrix, visited, predecessor);
        // Count vertices visited and print results if necessary
        for(int i = 0; i < v; i++){
            if( visited[i] != 0){
                totalVisited++;
            }
            if( printResults ){
                vertString +=  " " + String.valueOf(i);
                predString +=  " " + String.valueOf( predecessor[i] );
            }
        }
        if (printResults){
            dfsString += vertString + "\n" + "Predecessors: \n" + predString;
            System.out.println( dfsString );
        }
        return totalVisited;
    }

    /**
     * Matrix style DFS-Visit. Helps in the recurssive aspects of the DFS.
     *
     * @param v The number of vertices for the graph.
     * @param u The current vertex.
     * @param matrix The adjacency matrix of all vertices
     * @param visited The array used to 'mark' visited vertices
     * @param predecessor The array used to indicate a given vertex's predecessor
     */
    private static void matrixDFSVisit(int v, int u, int[][] matrix,
                                       int[] visited, int[] predecessor){
        visited[u] = 1;
        for (int i = 0; i < v; i++){
            if( ( matrix[u][i] != 0 ) && ( visited[i] == 0 ) ){
                predecessor[i] = u;
                matrixDFSVisit(v, i, matrix, visited, predecessor);
            }
        }
        visited[u] = 2;
    }

    /**
    * Method for adjacency style DFS. Uses an array to mark visited nodes.
    * This method is not implemented as it was not needed in this solution.
    * 
    * @param v The number of vertices for the graph
    * @param printResults If true the results will be printed
    * @return n The number of vertices visited.
    **/
    private static int adjDFS(int v, boolean printResults, 
                              LinkedList<Edge>[] adj){
        //StubyStub
        return 0;
    }

    /**
     * Prints out the adjacency matrix textually
     *
     * @param n Number of vertices in the graph
     * @param matrix The graph represented as an adjacency matrix
     */
    public static void printMatrix( int n, int[][] matrix){
        for(int i = 0; i < n; i++){
            for(int j =0; j < n; j++){
                System.out.print( String.valueOf( matrix[i][j] ) + "   ");
            }
            System.out.println();
        }
    }

     /**
      * Prints out the adjacency list textually
      *
      * @param n Number of vertices in the graph
      * @param adj The graph represented as an adjacency list
      */
    public static void printAdj( int n, LinkedList<Edge>[] adj){
        for(int i = 0; i < n; i++){
            System.out.print(String.valueOf(i) + "->" );
            for( Edge e : adj[i] ){
                System.out.format(" %d(%d)", e.getDestination(), 
                                  e.getWeight() );
            }
            System.out.println();
        }
    }
}
# README #

In order to get it running all you need to do is compile the source files and then run the main

### Summary ###

This is a command line tool that takes in 3 parameters and then does a series of graph manipulation tasks on them. To run the program you need to supply an input file in the form (n, seed, p) Where n is the number of vertices, and p is the chance of two vertices having a joining edge. The intended usage is as follows.

    java MST input1

Currently the program is setup to use packages and the usage is incorrect. This will be fixed in the next version

### Development info ###

This program is being developed for an Algorithms course. It's purpose is to analyze various graph manipulation tasks.

### Contact ###


Please contact alf40k@gmail.com or ArthurLLunn@gmail.com with any questions